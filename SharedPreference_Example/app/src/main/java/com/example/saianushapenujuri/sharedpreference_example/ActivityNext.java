package com.example.saianushapenujuri.sharedpreference_example;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityNext extends AppCompatActivity {

    private TextView mName;
    private TextView mPassword;
    private Button mLoad;
    private Button mPrevious;
    final String DEFAULT = "noValues";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        mName = findViewById(R.id.text_name);
        mPassword = findViewById(R.id.text_password);

        mLoad = findViewById(R.id.btn_load);
        mLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("Mydata", Context.MODE_PRIVATE);
                String name = sharedPreferences.getString("name", DEFAULT);
                String password = sharedPreferences.getString("password", DEFAULT);
                if (name.equals(DEFAULT) || password.equals(DEFAULT)) {
                    Toast.makeText(ActivityNext.this, "no data", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(ActivityNext.this, "data loaded", Toast.LENGTH_LONG).show();
                    mName.setText(name);
                    mPassword.setText(password);
                }
            }
        });
        mPrevious = findViewById(R.id.btn_previous);
        mPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityNext.this, MainActivity.class);
                Toast.makeText(ActivityNext.this, "previous", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
    }
}
